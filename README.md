# COMO DEFINIR OS MAPAS #

## Definindo o mapa inferior: ##

* Criar uma imagem de 20x20 pixels usando algum editor de imagens (paint funciona muito bem).

* A grama é definida pela cor RGB (0,255,0), quaisquer outros valores utilizados não serão válidos e não gerarão o mapa (prestar atenção!).

* A água é definida pela cor RGB (0,0,0).

* Nomear esse arquivo como “lowerMap” e salvar na pasta “DigKart/CodeBlocks/FPS/res”.


## Para definirmos o mapa superior, é preciso seguir os seguintes passos: ##

* Criar uma imagem com 20x20 pixels;

* Os inimigos são definidos pela cor RGB (255,0,0). Só podem haver quatro inimigos no mapa, extras serão deletados.

* As pedras (blocos superiores) são definidos pela cor RGB (127,127,127).

* As rachaduras são definidas pela cor RGB (90,50,50).

* Os buracos são definidos pela cor RGB (150,120,80).

* Nomear essa imagem como “upperMap” e salvar na pasta “DigKart/CodeBlocks/FPS/res”.

## TECLAS USAVEIS NO JOGO ##

* W: Movimenta o personagem para frente.

* S: Movimenta o personagem para trás.

* A: Rotaciona o personagem 90º para a esquerda.

* D: Rotaciona o personagem 90º para a direita.

* V: Muda entre as câmeras disponíveis no jogo.

* B: Empurra os inimigos 2 blocos para frente.

* R: Faz o Kart usar turbo.

* SPACE: Cria rachaduras.

* ESC: Sai do jogo.

* LMB: Cria buracos.

* RMB: Destrói buracos.

[Fonte, executavel e relatórios](http://www.inf.ufrgs.br/~cjstradolini/Fundamentos%20de%20Computa%c3%a7%c3%a3o%20Gr%c3%a1fica/Dig_Kart.zip)